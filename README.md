# Satheesh Interview Task NewsArticles


## Installation

Get clone the project from git using the below repository.

```
https://gitlab.com/satheeshUI/newsarticletask.git
```

Use the package manager [npm] to install packages.

```
cd newsarticle
npm install
```

## Run the Project using the below comment

```
npm start
```

## Build the project

```
npm run build
```
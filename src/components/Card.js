import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
    maxWidth: '100%',
    margin: 20
  },
  media: {
    height: 140,
  },
});

export default function MediaCard(props) {
  const classes = useStyles();
  const { singleArticle } = props

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={singleArticle.image}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            { singleArticle.title }
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            { singleArticle.author }
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            <b>Source</b>: { singleArticle.source } <br />
            <b>Data</b> : { singleArticle.date } <br />
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

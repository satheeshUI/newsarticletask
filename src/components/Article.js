import React from 'react';
import { useSelector } from 'react-redux';
import ArticleCard from './Card'

export default function Article(props) {
    const { singleArticle } = useSelector(state => {
        return {
            singleArticle: state.ArticleReducer.singleArticle
        }
    })
    return(
        <>
        {
            singleArticle && singleArticle.source ? <ArticleCard singleArticle={singleArticle}/> : null
        }
        </>
    )
}
import { combineReducers } from 'redux';
import ArticleReducer from './article/reducer';

const rootReducer = combineReducers({
    ArticleReducer
})

export default rootReducer;
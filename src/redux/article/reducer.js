const initialState = {
    notifyMsg: {},
    articlesList: {},
    singleArticle: {}
}

const ARTICLES_REDUCER = (state = initialState, action) => {
    const {type, payload} = action
    switch(type) {
        case 'SHOW_NOTIFICATION':
            return {
                ...state,
                notifyMsg: payload
            }
        case 'GET_ARTICLES':
            return {
                ...state,
                articlesList: payload.data
            }
        case 'SINGLE_ARTICLE':
            return {
                ...state,
                singleArticle: payload
            }
        default:
            return state
    }
}

export default ARTICLES_REDUCER
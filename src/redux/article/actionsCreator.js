import actions from './action';

const { getArticles, getArticle } = actions

const articleAction = payload => {
    return dispatch => {
        try {
            if(payload.isSingle)
                dispatch(getArticle(payload))
            else
                dispatch(getArticles(payload))
        }
        catch(err) {
            console.log(err, "error")
        }
    }
}

export default articleAction
import axios from 'axios';
import { articlesList } from './sample.json'
// const API_URI = "https://newsapi.org/v2/everything?q=tesla&from=2021-04-06&sortBy=publishedAt&apiKey=5cc774abf6bb4205b79876ec58d8a4b9"

const actions = {
    API_BEGIN: "API_BEGIN",
    GET_ARTICLES: 'GET_ARTICLES',
    SHOW_NOTIFICATION: 'SHOW_NOTIFICATION',
    SINGLE_ARTICLE: 'SINGLE_ARTICLE',

    getArticle: (payload) => {
        return {
            type: 'SINGLE_ARTICLE',
            payload: payload.data
        }
    },

    getArticles: (payload) => {
        return dispatch => {
            dispatch({
                type: 'GET_ARTICLES',
                payload: {data: articlesList}
            })
            // axios.get(API_URI).then(res => {
            //     dispatch({
            //         type: 'SHOW_NOTIFICATION',
            //         payload: {status: 'success', message: "Articles loaded successfully"}
            //     })
            //     dispatch({
            //         type: 'GET_ARTICLES',
            //         payload: {data: res.data}
            //     })
            // }).catch(err => {
            //     dispatch({
            //         type: 'SHOW_NOTIFICATION',
            //         payload: {status: 'failed', message: err}
            //     })
            // })
        }
    }
}

export default actions
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import EnhancedTable from './components/Table';
import articleAction from './redux/article/actionsCreator'
import Article from './components/Article';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function App()  {
  const dispatch = useDispatch();
  
  const { articlesList } = useSelector(state => {
    return {
      articlesList: state.ArticleReducer.articlesList
    }
  })
  
  useEffect(() => {
    dispatch(articleAction({action:'getarticle'}))
  }, [articlesList])


  const tableHeader = [
    { id: 'image', label: 'Image' },
    { id: 'source', label: 'Source' },
    { id: 'author', label: 'Author' },
    { id: 'title', label: 'Title' },
    { id: 'date', label: 'Date' },
    { id: 'url', label: 'URL' }
  ];

  const articleObj = e => {
    const { urlToImage, source, author, title, publishedAt, url } = e
    let obj = {
      image: urlToImage,
      source: source.name,
      author: author,
      title: title,
      date: publishedAt,
      url: url
    }
    return obj
  }

  const data = () => {
    let items = []
    if(articlesList.articles) {
      let iterate = articlesList.articles.map(articleObj)
      items = [...items, ...iterate]
    }
    return items
  }

  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/">
            <EnhancedTable tableHeader={tableHeader} rows={data()} />
          </Route>
          <Route exact path="/article">
            <Article />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
